local M = {}

local fn = vim.fn
local version = vim.version().minor
local utils = require("utils.modules")

local modes = {
  ["n"] = { "NORMAL", "StatusLineNormalMode" },
  ["niI"] = { "NORMAL i", "StatusLineNormalMode" },
  ["niR"] = { "NORMAL r", "StatusLineNormalMode" },
  ["niV"] = { "NORMAL v", "StatusLineNormalMode" },
  ["no"] = { "N-PENDING", "StatusLineNormalMode" },
  ["i"] = { "INSERT", "StatusLineInsertMode" },
  ["ic"] = { "INSERT (completion)", "StatusLineInsertMode" },
  ["ix"] = { "INSERT completion", "StatusLineInsertMode" },
  ["t"] = { "TERMINAL", "StatusLineTerminalMode" },
  ["nt"] = { "NTERMINAL", "StatusLineTerminalMode" },
  ["v"] = { "VISUAL", "StatusLineVisualMode" },
  ["V"] = { "V-LINE", "StatusLineVisualMode" },
  ["Vs"] = { "V-LINE (Ctrl O)", "StatusLineVisualMode" },
  [""] = { "V-BLOCK", "StatusLineVisualMode" },
  ["R"] = { "REPLACE", "StatusLineReplaceMode" },
  ["Rv"] = { "V-REPLACE", "StatusLineReplaceMode" },
  ["s"] = { "SELECT", "StatusLineSelectMode" },
  ["S"] = { "S-LINE", "StatusLineSelectMode" },
  [""] = { "S-BLOCK", "StatusLineSelectMode" },
  ["c"] = { "COMMAND", "StatusLineCommandMode" },
  ["cv"] = { "COMMAND", "StatusLineCommandMode" },
  ["ce"] = { "COMMAND", "StatusLineCommandMode" },
  ["r"] = { "PROMPT", "StatusLineConfirmMode" },
  ["rm"] = { "MORE", "StatusLineConfirmMode" },
  ["r?"] = { "CONFIRM", "StatusLineConfirmMode" },
  ["!"] = { "SHELL", "StatusLineTerminalMode" },
}

M.block = function()
  return "%#StatusLineBlock#" .. "▊"
end

M.system = function()
  local icon
  if vim.loop.os_uname().sysname == "Linux" then
    if utils.is_file("/etc/arch-release") or utils.is_file("/etc/artix-release") then
      icon = "%#StatusLineArchLinux#" .. "  "
    elseif utils.is_file("/etc/fedora-release") or utils.is_file("/etc/redhat-release") then
      icon = "%#StatusLineFedoraLinux#" .. "  "
    elseif utils.is_file("/etc/SuSE-release") then
      icon = "%#StatusLineSuseLinux#" .. "  "
    else
      icon = "%#StatusLineUbuntuLinux#" .. "  "
    end
  elseif
    vim.loop.os_uname().sysname == "FreeBSD"
    or vim.loop.os_uname().sysname == "NetBSD"
    or vim.loop.os_uname().sysname == "OpenBSD"
  then
    icon = "%#StatusLineBSD#" .. "  "
  elseif vim.loop.os_uname().sysname == "Darwin" then
    icon = "%#StatusLineDarwin#" .. "  "
  end
  return icon
end

M.mode = function()
  local m = vim.api.nvim_get_mode().mode
  local current_mode = "%#" .. modes[m][2] .. "#" .. "  " .. modes[m][1]
  local mode_sep1 = "%#" .. modes[m][2] .. "Sep" .. "#"
  return current_mode .. mode_sep1 .. "%#StatusLineBadWhiteSpace#"
end

M.fileInfo = function()
  local icon = "  "
  local filename = (fn.expand("%") == "" and "Empty ") or fn.expand("%:t")

  if filename == "NvimTree_1" then
    filename = "Empty "
  elseif vim.bo.filetype == "toggleterm" then
    filename = "Terminal"
  end

  if filename ~= "Empty " then
    local devicons_present, devicons = pcall(require, "nvim-web-devicons")

    if devicons_present then
      local ft_icon = devicons.get_icon(filename)
      icon = (ft_icon ~= nil and " " .. ft_icon) or ""
    end

    filename = " " .. filename .. " "
  end

  return "%#StatusLineFileInfo#" .. " " .. icon .. filename
end

M.git = function()
  if not vim.b.gitsigns_head or vim.b.gitsigns_git_status then
    return ""
  end

  local git_status = vim.b.gitsigns_status_dict

  local added = (git_status.added and git_status.added ~= 0) and ("%#StatusLineGitAdd#" .. "* " .. git_status.added)
    or ""
  local removed = (git_status.removed and git_status.removed ~= 0)
      and ("%#StatusLineGitRemoved#" .. " * " .. git_status.removed)
    or ""
  local changed = (git_status.changed and git_status.changed ~= 0)
      and ("%#StatusLineGitChanges#" .. " * " .. git_status.changed)
    or ""
  local branch_name = " " .. git_status.head .. " "

  return "%#StatusLinegitIcons#" .. "  " .. branch_name .. added .. removed .. changed
end

-- VirtualEnv
local function env_cleanup(venv)
  if string.find(venv, "/") then
    local final_venv = venv
    for w in venv:gmatch("([^/]+)") do
      final_venv = w
    end
    venv = final_venv
  end
  return venv
end

M.Venv = function()
  if vim.bo.filetype == "python" then
    local venv = os.getenv("CONDA_DEFAULT_ENV")
    if venv then
      return "%#StatusLinePythonVenv#   (" .. env_cleanup(venv) .. ")"
    end
    venv = os.getenv("VIRTUAL_ENV")
    if venv then
      return "%#StatusLinePythonVenv#   (" .. env_cleanup(venv) .. ")"
    end
    return ""
  end
  return ""
end

-- Npm
M.Npm = function()
  if utils.is_file("./package.json") then
    return "%#StatusLineNpm#   Nodejs"
  else
    return ""
  end
end

-- LSP STUFF
M.LSP_Diagnostics = function()
  if not rawget(vim, "lsp") then
    return ""
  end

  local errors = #vim.diagnostic.get(0, { severity = vim.diagnostic.severity.ERROR })
  local warnings = #vim.diagnostic.get(0, { severity = vim.diagnostic.severity.WARN })
  local hints = #vim.diagnostic.get(0, { severity = vim.diagnostic.severity.HINT })
  local info = #vim.diagnostic.get(0, { severity = vim.diagnostic.severity.INFO })

  ---@diagnostic disable-next-line: cast-local-type
  errors = errors and ("%#StatusLinelspError#" .. "󰅙 " .. errors .. " ") or ""
  ---@diagnostic disable-next-line: cast-local-type
  warnings = warnings and ("%#StatusLinelspWarning#" .. "  " .. warnings .. " ") or ""
  ---@diagnostic disable-next-line: cast-local-type
  hints = hints and ("%#StatusLinelspHints#" .. "󰌵 " .. hints .. " ") or ""
  ---@diagnostic disable-next-line: cast-local-type
  info = info and ("%#StatusLinelspInfo#" .. "󰋼 " .. info .. " ") or ""

  return "  " .. errors .. warnings .. hints .. info
end

M.LSP_status = function()
  local buf_client_names = {}
	if rawget(vim, "lsp") then
		for _, client in ipairs(vim.lsp.get_active_clients({ bufnr = 0 })) do
			if client.attached_buffers[vim.api.nvim_get_current_buf()] then
        table.insert(buf_client_names, client.name)
			end
		end
    local unique_client_names = table.concat(buf_client_names, ", ")
    local language_servers = string.format("%s", unique_client_names)
    return (vim.o.columns > 100 and "%#StatusLineLspStatus#" .. "   [" .. language_servers .. "] ")
      or "   [] "
	end
end

M.cursor_position = function()
  local left_sep = "%#StatusLinepos_sep#" .. "%#StatusLinepos_icon#" .. " "

  local current_line = fn.line(".")
  local total_line = fn.line("$")
  local text = math.modf((current_line / total_line) * 100) .. tostring("%%")

  text = (current_line == 1 and "Top") or text
  text = (current_line == total_line and "Bot") or text

  if vim.bo.filetype ~= "toggleterm" then
    return left_sep .. "%#StatusLinepos_text#" .. " " .. text .. " "
  else
    return "%#StatusLinepos_text#" .. "Commandline "
  end
end

-- https://github.com/NvChad/ui/blob/v2.5/lua/nvchad/stl/utils.lua#L116:
M.lsp_msg = function()
  if version < 10 then
    return ""
  end

  local msg = vim.lsp.status()

  if #msg == 0 or vim.o.columns < 120 then
    return ""
  end

  local spinners = { "", "󰪞", "󰪟", "󰪠", "󰪢", "󰪣", "󰪤", "󰪥" }
  local ms = vim.loop.hrtime() / 1e6
  local frame = math.floor(ms / 100) % #spinners

  return spinners[frame + 1] .. " " .. msg
end

return M
