local M = {}

function M.config()
  local modules = require("statusline.modules")

  dvim.core.statusline = {
    modules = {
      modules.block(),
      modules.system(),
      modules.git(),
      modules.mode(),
      modules.LSP_Diagnostics(),
      modules.Venv(),
      modules.Npm(),
      "%=",
      modules.lsp_msg(),
      "%=",
      modules.fileInfo(),
      modules.LSP_status() or "%#StatusLineLspStatus#   [] ",
      modules.cursor_position(),
      modules.block(),
    },
  }

  return table.concat(dvim.core.statusline.modules)
end

function M.setup()
  vim.fn.matchadd("error", "hello World")
  vim.o.statusline = "%!v:lua.require('statusline').config()"
end

return M
